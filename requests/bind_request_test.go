package requests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

type EchoRequestTest struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type GinRequestTest struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func TestBindRequest(t *testing.T) {
	asserttion := assert.New(t)
	e := echo.New()

	// test Echo framework.
	{ // bind data successfully.
		// arrange
		echoRequest := []EchoRequestTest{
			{
				ID:   1,
				Name: "Quoc Bang",
			},
			{
				ID:   2,
				Name: "Duc Thai",
			},
		}
		echoBody, err := json.Marshal(echoRequest)
		asserttion.NoError(err)

		req := httptest.NewRequest(http.MethodPost, "/echo-request-test", bytes.NewReader(echoBody))
		req.Header.Set("Content-Type", "application/json") // Set the content type to "application/json"
		rec := httptest.NewRecorder()
		echoContext := e.NewContext(req, rec)

		// act
		actual, err := BindRequest[[]EchoRequestTest](echoContext)

		// assert
		expected := echoRequest
		asserttion.NoError(err)
		asserttion.Equal(expected, actual)
	}

	// test Gin framework
	{ // bind data successfully.
		// arrange
		ginRequest := GinRequestTest{
			ID:   1,
			Name: "Quoc Bang",
		}
		ginBody, err := json.Marshal(ginRequest)
		asserttion.NoError(err)

		req := httptest.NewRequest(http.MethodPost, "/echo-request-test", bytes.NewReader(ginBody))
		req.Header.Set("Content-Type", "application/json") // Set the content type to "application/json"
		ginContext := gin.Context{
			Request: req,
		}

		// act
		actual, err := BindRequest[GinRequestTest](&ginContext)

		// assert
		expected := ginRequest
		asserttion.NoError(err)
		asserttion.Equal(expected, actual)
	}
	{ // not support bind slice for Gin framework.
		// arrange
		ginRequest := []GinRequestTest{
			{
				ID:   1,
				Name: "Quoc Bang",
			},
			{
				ID:   2,
				Name: "Duc Thai",
			},
		}
		ginBody, err := json.Marshal(ginRequest)
		asserttion.NoError(err)

		req := httptest.NewRequest(http.MethodPost, "/echo-request-test", bytes.NewReader(ginBody))
		req.Header.Set("Content-Type", "application/json") // Set the content type to "application/json"
		ginContext := gin.Context{
			Request: req,
		}

		// act
		_, err = BindRequest[[]GinRequestTest](&ginContext)

		// assert
		expected := fmt.Errorf("not support bind slice with Gin framework")
		asserttion.Error(err)
		asserttion.Equal(expected, err)
	}
}
