package requests

import (
	"fmt"
	"reflect"

	"github.com/gin-gonic/gin"
	"github.com/labstack/echo"
)

// BindRequest support for bind request json for multiple framework of Golang.
// framework supporting:
//   - Echo
//   - Gin
//
// Required:
//   - Gin: with this framework BindRequest need pointer of context,
//     because Gin context contains sync.RWMutex
func BindRequest[T any](ctx interface{}) (T, error) {
	var request T
	switch c := ctx.(type) {
	case echo.Context:
		if err := c.Bind(&request); err != nil {
			return request, err
		}
	case *gin.Context:
		if reflect.TypeOf(request).Kind() == reflect.Slice {
			return request, fmt.Errorf("not support bind slice with Gin framework")
		}
		if err := c.BindJSON(&request); err != nil {
			return request, err
		}
	default:
		return request, fmt.Errorf("unsupported context type")
	}
	return request, nil
}
