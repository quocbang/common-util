FROM golang:1.20.0-alpine3.16 as server_test

RUN apk add --no-cache git
RUN apk add --update --no-cache curl build-base
RUN apk update && apk add openssh

RUN go generate .
RUN go vet ./...
RUN go test -race -coverprofile .testCoverage.txt ./...
RUN go tool cover -func .testCoverage.txt